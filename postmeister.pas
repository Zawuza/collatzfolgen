{$mode objfpc}
{Threads controller}

uses
{$IFDEF UNIX}
cthreads,
cmem,
{$ENDIF}
SysUtils, Classes, Process, PostConfig;

{Thread class}
type TSequenceThread = class(TThread)
        protected
        procedure Execute; override;
        public
        Number:UINT64;
        Output:AnsiString;
        Status:Integer;
        constructor Create(CreateSuspended:boolean;const StackSize: SizeUInt = DefaultStackSize);
     end;
     
procedure TSequenceThread.Execute;
var command:string;
begin
    Writeln('Thread: ' + IntToStr(Number));
    command:=GetCurrentDir + '\folger ';
    while not RunCommand(command,[IntToStr(Number)],Output) do; {Call folger}
    if Output<>'' then Writeln(Output); {Check and show output}
    Status:=0; 
end;

constructor TSequenceThread.Create(CreateSuspended:boolean;const StackSize: SizeUInt = DefaultStackSize);
begin
  FreeOnTerminate:=false;
  inherited Create(CreateSuspended);
  Status:=1;
end;

{BODY OF PROGRAM ------------------------------------}
     
var Number, MinNumber, MaxNumber:UINT64;
    i:integer;
    Threads:array [1..CountOfProcesses] of TSequenceThread;
begin
    Writeln('Die Anzahl von Threads:' + IntToStr(CountOfProcesses));
    Writeln('Geben Sie bitte den Nummer ein, von dem ich beginnen soll');
    Writeln('Achtung!!! Diese Nummer wird mit ' + IntToStr(NumbersInThread) +
    ' multipliziert');
    Readln(MinNumber);

    Writeln('Und die Zahl, bis der (nicht inklusiv) ich suchen muss (Auch in Millionen, bitte) :)');
    Writeln('MAX soll um ' + IntToStr(CountOfProcesses) + ' groesser als MIN sein');
    Writeln('Achtung!!! Diese Nummer wird auch mit ' + IntToStr(NumbersInThread) +
        ' multipliziert!!!' );
    Readln(MaxNumber);
    
    {Check input}
    if MinNumber>(18446744073709551615 div NumbersInthread) then
      begin
        Writeln('Maximale Nummer ist zu gross');
        exit;
      end;
    if MaxNumber>(18446744073709551615 div NumbersInThread) then 
      begin
        Writeln('Minimale Nummer ist zu gross');
        halt;
      end;
    
    Number:=MinNumber*NumbersInThread;
    MaxNumber:=MaxNumber*NumbersInThread;
    
    Writeln('Beginne');
    {First creating of threads (Will be always executed)}
    for i:=1 to CountOfProcesses do
        begin
          Threads[i]:=TSequenceThread.Create(true);
          Threads[i].Output:='';
          Threads[i].Number:=Number;
          Threads[i].Start;
          Number:=Number+NumbersInThread;
        end;
{Check all threads in loop. If thread is free, it will work with next number }
    while true do
        begin
        if Number>=MaxNumber then break; 
        for i:=1 to CountOfProcesses do
          begin
            if Threads[i].Status=0 then
                begin
                  Threads[i].Destroy;
                  Threads[i]:=TSequenceThread.Create(true);
                  Threads[i].Output:='';
                  Threads[i].Number:=Number;
                  Threads[i].Start;
                  Number:=Number+NumbersinThread; {Number for next thread}
                end;  
          end;
        end;
{Terminating and destroying of threads }
Writeln('Beende das Programm');
for i:=1 to CountOfProcesses do
  begin
    Threads[i].Terminate; 
    Threads[i].Destroy;
  end;
writeln('Geschafft, Mann');        
end.
        