{$mode objfpc}
{Program, that checks sequences}
uses SysUtils, Classes, PostConfig;
var cur, {First number}
    max,  {Maximal length of sequence, that was find in this thread }
    maxzahl, {number with max sequence}
    {Not important:}
    folgen_x: UINT64;
    count, i,code:integer;
    List:TStringList;
    
begin     
    count:=0;  
    max:=0;
    maxzahl:=0;
    
    List:=TStringList.Create; 
    Val(ParamStr(1),cur,code); {Extract interval of thread}
    if code<>0 then 
        begin
        {It must be never true }
        Writeln('Kann nicht die Zahlen ab ',Paramstr(0),' pruefen');
        halt;
        end;
    
    for i:=0 to NumbersInThread-1 do
    begin
        count:=0;
        folgen_x:=i+cur;
        repeat 
            if folgen_x<1 then break;
            if (folgen_x mod 2)=0 then
                folgen_x:=folgen_x div 2
            else folgen_x:=(3*folgen_x)+1;
            Inc(count);
        until folgen_x=1;
        if count>max then 
            begin
            max:=count;
            maxzahl:=i+cur;
            end;
    end;
    try
    List.LoadFromFile('result.txt');  {Check numbers in file and if result of program is
            ,                            bigger, then write new result in file and show in 
                                         output}
    if (StrToInt(List.Strings[0])<max) then 
      begin
      List.Strings[0]:=IntToStr(max);
      List.Strings[1]:=IntToStr(maxzahl);
      List.SaveToFile('result.txt');
      Writeln('Neuer Rekord: ',max,' Bei der Zahl: ',maxzahl);
      end;
    finally
      List.Free; {Destructor}
    end
end.